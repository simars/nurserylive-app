// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'kinvey', 'starter.routes'])
    .run(function ($ionicPlatform, $kinvey) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });

        var promise = $kinvey.init({
            appKey: 'kid_-Jz34VfZpe',
            appSecret: 'ac65334064d041f7a5664e64c36ff268'
        });
        promise.then(function () {
            // Kinvey initialization finished with success
            console.log("Kinvey init with success");
            var user = $kinvey.getActiveUser();
            if (null === user) {
                return $kinvey.User.login({
                    username: 'test',
                    password: 'test'
                });
            } else {
                return user;
            }
        }).then(function (_user) {
            console.log("Kinvey got user " + JSON.stringify(_user));
        }, function (errorCallback) {
            // Kinvey initialization finished with error
            console.log("Kinvey init with error: " + JSON.stringify(errorCallback));
        })

    }).config(['$ionicConfigProvider', function ($ionicConfigProvider) {

    $ionicConfigProvider.tabs.position('bottom'); // other values: top

}]);










