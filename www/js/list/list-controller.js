angular.module('list', ['ionic','kinvey'])
    .controller('ListController', ['$scope', '$http', '$state', '$ionicModal', '$kinvey',
    function($scope, $http, $state,$ionicModal, $kinvey) {
        var promise = $kinvey.DataStore.find('Item');
        promise.then(function(data) {
            $scope.items = data;
            $scope.whichitem=$state.params.aId;
            $scope.data = { showDelete: false, showReorder: false };

            $scope.onItemDelete = function(item) {
                $kinvey.DataStore.destroy("Item", item._id).then(function() {
                    $scope.items.splice($scope.items.indexOf(item), 1);
                },function(err) {
                    console.log('[$delete] received error: ' + JSON.stringify(err, null, 2));
                });
            };

            $scope.doRefresh =function() {
                $kinvey.DataStore.find('Item').then(function(data) {
                    $scope.items = data;
                    $scope.$broadcast('scroll.refreshComplete');
                }, function(err) {
                    console.log('[$query] received error: ' + JSON.stringify(err, null, 2));
                });
            };

            $scope.toggleStar = function(item) {
                item.star = !item.star;
            };

            $scope.moveItem = function(item, fromIndex, toIndex) {
                $scope.items.splice(fromIndex, 1);
                $scope.items.splice(toIndex, 0, item);
            };

            $ionicModal.fromTemplateUrl('templates/form.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modal = modal;
            });
            $scope.openModal = function(){
                $scope.modal.show();
                $scope.item = {};
            } ;
            $scope.closeModal = function(){
                $scope.modal.hide();
                $scope.item = {};
                delete $scope.fileModal;
                delete $scope.fileUrl;
            };

            $scope.submitModal = function() {
                var item = {
                    title: $scope.item.title,
                    location: $scope.item.location,
                    price: $scope.item.price,
                    description: $scope.item.description,
                    thumbnailUrl: $scope.fileUrl,
                    fileObject: {
                        _type: 'KinveyFile',
                        _id: $scope.fileModel._id
                    }
                };
                $kinvey.DataStore.save('Item', item).then(function(savedItem) {
                    $scope.items.unshift(savedItem);
                    $scope.closeModal();
                },function error(err) {
                    console.log('[$Save] received error: ' + JSON.stringify(err, null, 2));
                } );
            };

            $scope.openFileDialog=function() {
                console.log('fire! $scope.openFileDialog()');
                var fileElement = document.getElementById('file');
                fileElement.addEventListener('change', function (e) {
                    var theFile = e.target.files[0];

                    // save the file object

                    var promise = $kinvey.File.upload(theFile, {
                        _filename: theFile.name,
                        public: true,
                        size: theFile.size,
                        mimeType: theFile.type
                    }).then(function (_fileData) {
                        console.log("[$upload] success: " + JSON.stringify(_fileData, null, 2));
                        $scope.fileModel = _fileData;
                        var promise = $kinvey.File.stream(_fileData._id);
                        promise.then(function(file) {
                            $scope.fileUrl = file._downloadURL;
                        });
                    }, function error(err) {
                        console.log('[$upload] received error: ' + JSON.stringify(err, null, 2));
                    });
                }, false);
                ionic.trigger('click', { target: fileElement });
            };

        },function(err) {
            console.log('[$query] received error: ' + JSON.stringify(err, null, 2));
        });
    }]);