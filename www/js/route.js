angular.module('starter.routes', ['list','calender','submit'])
.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('tabs', {
            url: '/tab',
            abstract: true,
            templateUrl: 'templates/tabs.html'
        })

        .state('tabs.home', {
            url: '/home',
            views: {
                'home-tab' : {
                    templateUrl: 'templates/home.html'
                }
            }
        })

        .state('tabs.list', {
            url: '/list',
            views: {
                'list-tab' : {
                    templateUrl: 'templates/list.html',
                    controller: 'ListController'
                }
            }
        })

        .state('tabs.detail', {
            url: '/list/:aId',
            views: {
                'list-tab' : {
                    templateUrl: 'templates/detail.html',
                    controller: 'ListController'
                }
            }
        })

        .state('tabs.store', {
            url: '/store',
            views: {
                'store-tab' : {
                    templateUrl: 'templates/browse.html',
                    controller: 'StoreController'
                }
            }
        })

        .state('tabs.submit', {
            url: '/submit',
            views: {
                'submit-tab' : {
                    templateUrl:  'templates/submit.html',
                    controller: 'SubmitCtrl'
                }
            }
        });


    $urlRouterProvider.otherwise('/tab/home');
});