angular.module('calender', ['ionic', 'kinvey'])
    .controller('StoreController', ['$scope', '$http', '$state',
        function ($scope, $http, $state) {
            $http.get('js/data.json').success(function (data) {
                $scope.store = data.store;

                $scope.onItemDelete = function (dayIndex, item) {
                    $scope.store[dayIndex].collection.splice($scope.store[dayIndex].collection.indexOf(item), 1);
                };

                $scope.doRefresh = function () {
                    $http.get('js/data.json').success(function (data) {
                        $scope.store = data.store;
                        $scope.$broadcast('scroll.refreshComplete');
                    });
                };

                $scope.toggleStar = function (item) {
                    item.star = !item.star;
                };

            });
        }]);